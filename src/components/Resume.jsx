import './css/resume.css';


const Resume = () => {
    return (
    <div className="rela-block page">
        <div className="rela-block top-bar">
            <div className="caps name">
                <div className="abs-center">Samantha Hartig
                <p></p>
                <p>Greater Tampa Bay Area</p>
                <p>samanthahartig@gmail.com</p>
                <p>linkedin.com/in/hartigsa</p>
                </div>
            </div>
        </div>
        <div className="side-bar">

                <p className="rela-block caps side-header">
                    Technical Skills
                </p>
                <p className="rela-block list-thing">
                    Python, JavaScript, HTML5, CSS
                </p>
                <p className="rela-block list-thing">
                    React, React Hooks, Redux Toolkit
                </p>
                <p className="rela-block list-thing">
                    Django, PostgreSQL, MySQL, FastAPI
                </p>
                <p className="rela-block list-thing">
                    Insomnia, Docker, VMWare
                </p>
                <p className="rela-block list-thing">
                    PLC, SCADA, OIT, HMI
                </p>
                <p className="rela-block caps side-header">
                    Education
                </p>
                <p className="rela-block list-thing">
                    M.S. Software Engineering
                </p>
                <p className="rela-block list-thing">
                    B.S Graphic Design
                </p>
                <p className="rela-block list-thing">
                    A.A.S. Electrical Tech
                </p>
                <p className="rela-block list-thing">
                    Software Engineering Cert
                </p>
        </div>
        <div className="rela-block content-container">
            <h2 className="rela-block caps title">
                Engineer
            </h2>

            <div className="rela-block separator"></div>

            <div className="rela-block caps greyed">
                Profile
            </div>
            <p className="long-margin">
                I have honed my technical abilities and project management skills during over 8 years of engineering experience. I have been involved in all stages of project life cycles: from writing proposals, estimating, programming, installation, and troubleshooting.
            </p>
            <hr />

            <div className="rela-block caps greyed">
                Experience
            </div>
            <hr />

            <h3>Systems Engineer</h3>
                <p className="light">
                    General Control Systems, Inc | Mar 2018 – May 2023
                </p>
            <h4>Engineering/QA:</h4>
                <p className="justified">
                    • Designed user interfaces for real-time monitoring and data visualization, incorporating user feedback to emphasize UX, interactivity, and responsive design.
                    <br />
                    • Engineered data collection processes from multiple inputs, integrating front-end interfaces with back-end systems and databases, ensuring real-time data synchronization and system reliability.
                    <br />
                    • Interpreted system requirements from electrical and site drawings and technical specifications.
                    <br />
                    • Executed rigorous testing procedures, including FAT (Factory Acceptance Testing) and SAT (Site Acceptance Testing), to guarantee the delivery of high-quality projects.
                    </p>
            <h4>Project Management:</h4>
                <p className="justified">
                • Managed contracts up to $600K each, coordinating up to 6 projects simultaneously for timely delivery.
                </p>
            <h4>Mentoring, Training, Morale: </h4>
                    <p className="justified">
                    • Led remote deployment solutions and provided phone support for junior engineers during on-site troubleshooting.
                    <br />
                    • Designed and authored training materials, leading the onboarding and education of new team members.
                    <br />
                    • Planned monthly fun events and end of year event.
                    </p>
            <h4>Cross-functional Collaboration:</h4>
                    <p className="justified">
                    • Collaborated with clients to identify areas needing enhancement during routine maintenance, system upgrades, and other planned projects ensuring customer requirements were met.
                    <br />
                    • Collaborated with sales and technical teams to grasp customer needs, authoring compelling project proposals valued up to $1M, which contributed significantly to business revenue.
                    <br />
                    • Instrumental in the establishment of a new branch in Florida, generating significant client base expansion through strategic business development and securing lucrative contracts contributing to company growth.
                    </p>
            <h4>Documentation: </h4>
                    <p className="justified">
                    • Compiled project submittals and technical documentation, ensuring transparency and accurate record-keeping for stakeholders.
                    <br />
                    • Compiled bill of materials, datasheets, I/O lists.</p>
            <h4>Continuous Learning:</h4>
                    <p className="justified">
                    • Engaged in continuous professional development, actively pursuing industry-related education and certifications, keeping up-to-date with the latest trends and technologies.
                    </p>
                <br />

            <h3>Engineering Technologist</h3>
            <p className="light">Naval Nuclear Laboratory (FMP) | Jan 2015 – Mar 2018
            </p>
            <p className="justified">
                • Developed and integrated software solutions for Schneider Electric PLCs and HMIs.
                <br />
                • Oversaw and updated lab software and electronics, focusing on maximizing system performance and uptime.
                <br />
                • Orchestrated the acquisition and deployment of desktop hardware and software, supporting the technical needs of a 100+ member team.
                </p>
                <br />

            <h3>Aviation Electricians Mate</h3>
            <p className="light">US Navy | Mar 2010 – Mar 201</p>
            <p className="justified">
                • Oversaw and ensured the reliability of complex electrical systems for EA-6B Prowler aircraft, emphasizing system diagnostics and timely maintenance.
            </p>
            <hr />
            <div className="rela-block caps greyed">Education</div>
            <hr />
                <h3>West Virginia University</h3>
                <p>Masters of Science (M.S.), Software Engineering | 2025</p>

                <h3>SUNY New Paltz</h3>
                <p>Bachelor of Science (B.S.), Design and Visual Communications | 2009</p>

                <h3>Fulton-Montgomery Community College</h3>
                <p>Associate of Applied Science (A.A.S.) Electrical Technology | 2014</p>

                <h3>Galvanize Inc</h3>
                <p>774 hour certificate, Software Engineering | 2023</p>

                <h3>CISCO</h3>
                <p>Python Essentials 1 & 2</p>

                <hr />

            <div className="rela-block caps greyed">Other</div>
            <hr />
                <h3>Gold Award</h3>
                <p>Girl Scouts of the USA</p>

                <h3>Lego League Mentor</h3>
                <p>FIRST</p>

                <h3>SeaPerch Mentor</h3>
                <p>Office of Naval Research</p>

                <h3>Employee of the Year</h3>
                <p>General Control Systems</p>

            </div>

        </div>
    );
};

export default Resume;

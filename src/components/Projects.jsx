import React from 'react';
import './css/projects.css';
import violet from "./img/drinks.png";
import recipies from "./img/recipie.png";
import games from "./img/gamesmario.png";
import cars from "./img/carcar.png";
import python from "./img/python.png";
import gitlab from "./img/gitlab.png";


const projectData = [
    {
        project_name: "Violet",
        tech: "FastAPI and React, Postgres, authentication",
        about: "An app that makes use of https://www.thecocktaildb.com/ API. Get Random NA Uses the filter by alcoholic endpoint to get a list of drink IDs for both NA and Alcohol optional, combines the list, then selects a random ID from that list, uses that ID to hit the get drink by id endpoint from the third party api, displaying information for that drink and allowing the user to save it to the database. By Glass Uses the dictionary of glasses to display an image for the glass names. When the glass is clicked on it uses the glass name for the filter by glass endpoint. Picks a random drink ID from that list, uses that ID to hit the get drink by id endpoint from the third party api, displaying information for that drink and allowing the user to save it to the database.",
        image: violet,
        link: "https://gitlab.com/samanthahartig/violetproject"
    },
    {
        project_name: "Python",
        tech: "Python & JS",
        about: "Python and JS practice problems",
        image: python,
        link: "https://gitlab.com/samanthahartig/python-practice-problems/-/tree/first-pass"
    },
    {
        project_name: "Good Games",
        tech: "FastAPI and React, Postgres",
        about: "An app to review video games. Video game data is pulled in through use of the Rawg API. Users can sign in, review video games, and edit their profile. ",
        image: games,
        link: "https://gitlab.com/cenicz/good-games"
    },
    {
        project_name: "Cars",
        tech: "Django and React",
        about: "The sales microservice has a React frontend allowing the users to display and edit selected data. Items that can be added and deleted: customers, sales, salespersons. Items that can be viewed: customer list, salespersons list, sales list, sales history filtered by salesperson. The AutomobileVO will get data from the inventory automobile entity by use of the sales poller. Sales Record: -price -sales_person -customer -AutomobileVO -phone number Sales Person:-name -employee id AutomobileVO: -VIN -color -year -model",
        image: cars,
        link: "https://gitlab.com/samanthahartig/project-beta"
    },
    {
        project_name: "Recipies",
        tech: "Django",
        about: "A Django practice CRUD project",
        image: recipies,
        link: "https://gitlab.com/samanthahartig/scrumptious-recipes"
    }

];

const ProjectCard = ({ project }) => {
    return (
        <div className="wrap animate pop">
            <div className="overlay">
                <div className="overlay-content animate slide-left delay-2">
                    <h1 className="animate slide-left pop delay-4">{project.project_name}</h1>
                    <p className="animate slide-left pop delay-5" style={{  marginBottom: '2.5rem' }}>
                        {project.tech}
                    </p>
                </div>
                <div className="image-content animate slide delay-5" style={{ backgroundImage: `url(${project.image})` }}></div>
                <div className="dots animate">
                    <div className="dot animate slide-up delay-6"></div>
                    <div className="dot animate slide-up delay-7"></div>
                    <div className="dot animate slide-up delay-8"></div>
                </div>
            </div>
            <div className="text">
                <div className="code-container">
                    {project.link ?
                        <>
                            <strong>code:</strong>
                            <a href={project.link} target="_blank" rel="noopener noreferrer">
                                <img className="inset" src={gitlab} alt="GitLab Project" />
                            </a>
                        </>
                        :
                        <img className="inset" src={project.image} alt={project.project_name} />
                    }
                </div>
                <p>{project.about}</p>
            </div>
        </div>
    );
}

const Projects = () => {
    return (
        <div>
            {projectData.map(project => (
                <ProjectCard key={project.project_name} project={project} />
            ))}
        </div>
    );
}


export default Projects;

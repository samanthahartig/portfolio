import React, { useState } from 'react';
import './css/form.css';
import linkedin from "./img/linkedin.png";

const FORM_URL = import.meta.env.VITE_REACT_APP_FORM_URL;

const MyForm = () => {
    const [isSubmitted, setIsSubmitted] = useState(false);
    const [hasError, setHasError] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const formData = new FormData(event.target);
        try {
            const response = await fetch(FORM_URL, {
                method: 'POST',
                body: formData,
            });

            if (response.ok) {
                console.log("Form submitted successfully.");
                setIsSubmitted(true);
            } else {
                console.error("Error submitting form:", response.statusText);
                setHasError(true);
            }
        } catch (error) {
            console.error("Submission error:", error);
            setHasError(true);
        }
    };

    const renderFormError = () => {
        if (hasError) {
            return (
                <div>
                    <h2>Submission Error</h2>
                    <p>There was an error submitting the form. You can reach out directly at <a href="mailto:samanthahartig@gmail.com">samanthahartig@gmail.com</a>.</p>
                </div>
            );
        }
        return null;
    };

    const renderFormInputs = () => {
        if (isSubmitted) {
            return (
                <div>
                    <h2>Thank You!</h2>
                    <p>Your message has been sent successfully.</p>
                </div>
            );
        }

        return (
            <form action={FORM_URL} method="POST" onSubmit={handleSubmit}>
                <div>
                    <label>Name:</label>
                    <input type="text" name="name" />
                </div>
                <div>
                    <label>Email:</label>
                    <input type="email" name="email" />
                </div>
                <div>
                    <label>Message:</label>
                    <textarea ref={el => el && el.focus()}></textarea><br />
                </div>
                <input type="hidden" name="_gotcha" style={{ display: 'none' }} />
                <button type="submit">Send</button>
            </form>
        );
    };

    return (
        <div>
            <ol className="form-container">
                <li className="form-content-container">
                    <h1>Contact Me</h1>
                    <div className="descr">The best way to contact me is through LinkedIn</div>
                    <div className="descr"><a href="https://www.linkedin.com/in/hartigsa/" target="_blank" rel="noopener noreferrer">linkedin.com/in/hartigsa</a></div><br />
                    <div className="formicon">
                        <a href="https://www.linkedin.com/in/hartigsa/" target="_blank" rel="noopener noreferrer">
                            <img src={linkedin} alt="LinkedIn" />
                        </a>
                    </div>
                </li>
            </ol>
            <ol className="form-container">
                <li className="form-content-container">
                    <h1>Email Me</h1>
                    <div className="descr">This form is through getform.io and has a monthly limit.</div><br />
                    {renderFormInputs()}
                    {renderFormError()}
                </li>
            </ol>
        </div>
    );
};

export default MyForm;

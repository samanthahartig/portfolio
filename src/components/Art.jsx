import React from 'react';
import './css/art.css';
import art1 from "./img/art1.png";
import art2 from "./img/art2.png";
import art4 from "./img/art4.png";
import art5 from "./img/art5.jpg";
import art6 from "./img/art6.jpg";
import art7 from "./img/art7.jpg";
import art10 from "./img/art10.jpg";
import art11 from "./img/art11.jpg";
import art12 from "./img/art12.jpg";
import art13 from "./img/art13.jpg";
import art14 from "./img/art14.jpg";
import art15 from "./img/art15.jpg";


const Art = () => {
    return (
        <div className="art-container">
            { [art15, art2, art7, art4, art5, art6, art1, art10, art11, art12, art13, art14].map((art, index) => (
                <div className="art-box" key={index}>
                    <div className="art-card">
                        <img src={art} alt={`Art ${index}`} />
                    </div>
                </div>
            )) }
        </div>
    );
};

export default Art;

import React, { useState } from 'react';
import { Link } from "react-router-dom";
import linkedin from "./img/linkedin.png";
import gitlab from "./img/gitlab.png";
import './css/nav.css';

const Navbar = () => {
    const [isOpen, setIsOpen] = useState(false);
    const handleCloseMenu = () => {
        setIsOpen(false);
    };

    return (
        <nav className="navbar">
            <div className="menu-icon" onClick={() => setIsOpen(!isOpen)}>
                ☰
            </div>
            <ul className={`navbar-list ${isOpen ? 'open' : ''}`}>
                <div className="text-items">
                <li className="navbar-item-text"><Link to="/portfolio/" onClick={handleCloseMenu}>HOME</Link></li>
                <li className="navbar-item-text"><Link to="/portfolio/resume" onClick={handleCloseMenu}>RESUME</Link></li>
                <li className="navbar-item-text"><Link to="/portfolio/contact" onClick={handleCloseMenu}>CONTACT</Link></li>
                <li className="navbar-item-text"><Link to="/portfolio/projects" onClick={handleCloseMenu}>TECH</Link></li>
                <li className="navbar-item-text"><Link to="/portfolio/art" onClick={handleCloseMenu}>ART</Link></li>
                </div>
                <div className="icon-items">
                    <li className="navbar-item icon">
                        <a href="https://gitlab.com/samanthahartig" target="_blank" rel="noopener noreferrer">
                            <img src={gitlab} alt="GitLab" />
                        </a>
                    </li>
                    <li className="navbar-item icon">
                        <a href="https://www.linkedin.com/in/hartigsa/" target="_blank" rel="noopener noreferrer">
                            <img src={linkedin} alt="LinkedIn" />
                        </a>
                    </li>
                </div>
            </ul>
        </nav>
    );
};

export default Navbar;

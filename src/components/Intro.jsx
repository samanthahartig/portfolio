import React, { useEffect } from 'react';
import topimg from "./img/headerimg.jpg";
import me from "./img/lightmode.png";
import './css/intro.css';


const Intro = () => {
        useEffect(() => {
            const script = document.createElement('script');
            script.src = "//my.visme.co/visme-embed.js";
            script.async = true;
            document.body.appendChild(script);

            return () => {
                document.body.removeChild(script);
            }
        }, []);



    return (
        <div className="overview-container">
        <div className="banner-container">
            <img src={topimg} alt="Background" className="background-image" />
                <div className="text-samantha">
                    SAMANTHA    HARTIG
                </div>
        </div>
            {/* <div className="text-hartig">
                HARTIG
            </div> */}

        <div className="headshot-flex">
            <div className="image-me">
                <img src={me} alt="headshot" />
            </div>
            <div className="text-left">
                <h1 className="hello">Hello! I'm Samantha</h1>
                <div className="h4-container">
                    <h4>ENGINEER</h4>
                    <h4>DESIGNER</h4>
                    <h4>ARTIST</h4>
                </div>
            </div>
        </div>

        <div className="over-content-container">
            <ol>
                <li style={{"--accent-color": "#a05d54"}}>
                {/* <div className="icon"><i class="fa-light fa-lightbulb-exclamation-on"></i></div> */}
                <div className="title">Languages</div>
                <div className="descr">Python, JavaScript, SQL, HTML5, CSS</div>
                </li>
                <li style={{"--accent-color": "#675651"}}>
                {/* <div className="icon"><i class="fa-light fa-lightbulb-exclamation-on"></i></div> */}
                <div className="title">Frontend</div>
                <div className="descr">React, React Hooks, Redux Toolkit</div>
                </li>
                <li style={{"--accent-color": "#eaac77"}}>
                {/* <div className="icon"><i class="fa-light fa-lightbulb-exclamation-on"></i></div> */}
                <div className="title">Backend & DB</div>
                <div className="descr">Django, PostgreSQL, MySQL, FastAPI</div>
                </li>
                <li style={{"--accent-color": "#3c4c4d"}}>
                {/* <div className="icon"><i class="fa-light fa-lightbulb-exclamation-on"></i></div> */}
                <div className="title">Tools </div>
                <div className="descr">Insomnia, Docker, VMWare</div>
                </li>
                <li style={{"--accent-color": " #e6865b"}}>
                {/* <div className="icon"><i class="fa-light fa-lightbulb-exclamation-on"></i></div> */}
                <div className="title">Industrial Systems</div>
                <div className="descr">PLC, SCADA, OIT, HMI</div>
                </li>
                <li style={{"--accent-color": "#4f3428"}}>
                {/* <div className="icon"><i class="fa-light fa-lightbulb-exclamation-on"></i></div> */}
                <div className="title">Other</div>
                <div className="descr">Project Management, Customer Support, Training, Morale Event Planning, Troubleshooting, Project Documentation</div>
                </li>
            </ol>
        </div>

        <div className="visme-embed-container">
                    <div className="visme_d"
                            data-title="Paper Company History Timeline Roadmap"
                            data-url="pv4pw0wv-untitled-project"
                            data-w="800"
                            data-full-h="false"
                            data-h="1300"
                            data-domain="my">
                    </div>
                    <p style={{ width: '220px', borderRadius: '3px', padding: '3px', fontSize: '12px', fontFamily: 'Arial, sans-serif', color: '#314152', whiteSpace: 'nowrap' }}>

                        <a href="https://www.visme.co/make-infographics?utm_source=CTA&amp;utm_medium=Embed"
                            target="_blank"
                            rel="noreferrer"
                            style={{ fontWeight: '600', textDecoration: 'none', fontSize: '12px', fontFamily: 'Arial, sans-serif', color: '#314152', whiteSpace: 'nowrap' }}>

                        </a>
                    </p>
                </div>

        </div>
    );
}
export default Intro;

import React from "react";
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import './App.css'
import Navbar from './components/Navbar'
import Intro from './components/Intro'
import Resume from './components/Resume'
import MyForm from './components/MyForm'
import Projects from "./components/Projects";
import Art from "./components/Art";
import dwg from "./components/img/dwg.png"

function App() {


  return (

    <div className="App">
      <BrowserRouter>
      <Navbar />
      <Routes>
      <Route path="/portfolio/" element={<Intro />} />
      <Route path="/portfolio/resume" element={<Resume />} />
      <Route path="/portfolio/contact" element={<MyForm />} />
      <Route path="/portfolio/projects" element={<Projects />} />
      <Route path="/portfolio/art" element={<Art />} />
      </Routes>
      </BrowserRouter>
      <div>
                <img src={dwg} alt="new paltz outline" className="bottom-image" />
            </div>
    </div>
  );
}

export default App
